import React from 'react';

class IconFooter extends React.Component{
    render(){
        return(
            <>
            <a href="http://freewebsitetemplates.com/go/facebook/" target="_blank" class="facebook"></a> <a href="http://freewebsitetemplates.com/go/twitter/" target="_blank" class="twitter"></a> <a href="http://freewebsitetemplates.com/go/googleplus/" target="_blank" class="googleplus"></a>
            </>
        )
    }
}
export default IconFooter;