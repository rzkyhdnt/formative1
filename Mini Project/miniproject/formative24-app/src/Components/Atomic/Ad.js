import React from "react";

class Ad extends React.Component {
  render() {
    return (
      <li>
        <a href="">
          <img src={this.props.image} alt="Img" />
        </a>
      </li>
    );
  }
}

export default Ad;
