import React from "react";

class Button extends React.Component {
  render() {
    return (
      <>
        <a href="" class="more">
          {this.props.buttonName}
        </a>
      </>
    );
  }
}
export default Button;
