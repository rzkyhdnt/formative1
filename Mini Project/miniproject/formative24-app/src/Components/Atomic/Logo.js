import React from 'react';
import logo from "../../Assets/images/logo.png";

class Logo extends React.Component{
    render(){
        return(
            <a href="index.html" id="logo">
            <img src={logo} alt="LOGO" />
          </a>
        )
    }
}

export default Logo;