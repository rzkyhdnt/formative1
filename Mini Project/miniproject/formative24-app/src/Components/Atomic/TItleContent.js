import React from "react";

class TitleContent extends React.Component {
  render() {
    return (
      <>
        <h1>{this.props.title}</h1>
      </>
    );
  }
}
export default TitleContent;