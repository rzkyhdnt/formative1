import React from "react";
import Button from "./Button";

class BodyContent extends React.Component {
  render() {
    return (
      <>
        <p>{this.props.body}</p>
      </>
    );
  }
}
export default BodyContent;
