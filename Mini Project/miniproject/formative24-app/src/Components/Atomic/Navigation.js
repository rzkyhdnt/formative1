import React from "react";

class Navigation extends React.Component {
  render() {
    return (
      <li>
        <a href={this.props.href}>
          <span>{this.props.navItem}</span>
        </a>
      </li>
    );
  }
}

export default Navigation;
