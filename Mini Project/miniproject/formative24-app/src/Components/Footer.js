import React from "react";
import BodyFooter from "./Atomic/BodyFooter";
import IconFooter from "./Atomic/IconFooter";
import Search from "./Atomic/Search";

class Footer extends React.Component {
  render() {
    return (
      <div id="footer">
        <div class="background">
          <div class="body">
            <div class="subscribe">
              <h3>Get Weekly Newsletter</h3>
              <Search />
            </div>
            <div class="posts">
              <h3>Latest Post</h3>
              <BodyFooter />
            </div>
            <div class="connect">
              <h3>Follow Us:</h3>
              <IconFooter />
            </div>
          </div>
        </div>
        <span id="footnote">
          <a href="index.html">Moonstrosity Custom Shirts</a> &copy; 2012 | All
          Rights Reserved.
        </span>
      </div>
    );
  }
}

export default Footer;
