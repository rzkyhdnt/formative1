import React from "react";
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import "bootstrap/dist/css/bootstrap.css";

class AdBox extends React.Component {
  state = {
    data: [],
  };

  async componentDidMount() {
    const response = await fetch("http://localhost:8080/list/content");
    const body = await response.json();
    this.setState({ data: body });
  }

  render() {
    const { data } = this.state;
    return (
      <div id="adbox">
        <h1>Hot Shirts for this Month</h1>
        <ul>
          <Carousel
            additionalTransfrom={0}
            autoPlay={true}
            arrows={true}
            autoPlaySpeed={3000}
            centerMode={false}
            className="container-with-dots"
            dotListClass=""
            draggable={false}
            focusOnSelect={true}
            infinite
            itemClass=""
            keyBoardControl
            minimumTouchDrag={80}
            renderButtonGroupOutside={false}
            renderDotsOutside={false}
            showDots
            sliderClass=""
            slidesToSlide={1}
            slidesToShow={1}
            swipeable
            responsive={{
              desktop: {
                breakpoint: {
                  max: 3100,
                  min: 1024,
                },
                items: 3,
              },
              mobile: {
                breakpoint: {
                  max: 464,
                  min: 0,
                },
                items: 3,
              },
              tablet: {
                breakpoint: {
                  max: 1024,
                  min: 464,
                },
                items: 3,
              },
            }}
          >
            {data.map((data) => (
              <img
                className=""
                src={data.imgurl}
                alt="First slide"
                width={288}
              ></img>
            ))}
          </Carousel>
        </ul>
      </div>
    );
  }
}
export default AdBox;
