import React from "react";
import Logo from "../Components/Atomic/Logo"
import Nav from "../Components/Atomic/Navigation";

class HeadNav extends React.Component {
  state = {
    navPrimary: ["Home", "About", "Men"],
    navSecondary: ["Women", "Blog", "Contact"],
  };
  render() {
    const { navPrimary, navSecondary } = this.state;
    return (
      <div>
        <div id="navigation">
          <div class="infos">
            <a href="index.html">Cart</a> <a href="index.html">0 items</a>
          </div>
          <div>
            <a href="index.html">Login</a> <a href="index.html">Register</a>
          </div>
          <ul id="primary">
            {navPrimary.map((item) => (
              <Nav navItem={item} href={item.toLowerCase() + ".html"}></Nav>
            ))}
          </ul>
          <ul id="secondary">
            {navSecondary.map((item) => (
              <Nav navItem={item} href={item.toLowerCase() + ".html"}></Nav>
            ))}
          </ul>
          <Logo />
        </div>
      </div>
    );
  }
}

export default HeadNav;