import React from "react";
import BodyContent from "./Atomic/BodyContent";
import Button from "./Atomic/Button";
import TitleContent from "./Atomic/TItleContent";

class MainContent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      card: [],
      currentPage: 1,
      imagesPerPage: 1  ,
    };
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(event) {
    this.setState({
      currentPage: Number(event.target.id),
    });
  }

  async componentDidMount() {
    const response = await fetch("http://localhost:8080/list/content");
    const body = await response.json();
    this.setState({ card: body });
  }

  renderMenu() {
    let dataList = new Array();
    const { card, currentPage, imagesPerPage } = this.state;
    const indexOfLastImages = currentPage * imagesPerPage;
    const indexOfFirstImages = indexOfLastImages - imagesPerPage;
    const currentImages = card.slice(indexOfFirstImages, indexOfLastImages);
    for (let iter = 0; iter < currentImages.length; iter++) {
      let imageUrl = currentImages[iter].title;
      let name = currentImages[iter].body;
      let price = currentImages[iter].button;
      dataList.push(
        <>
          <div id="body">
            <div id="contents">
              <ul id="articles">
                {card.map((item) => (
                  <li>
                    <TitleContent title={item.title} />
                    <BodyContent body={item.body} />
                    <Button buttonName={item.button} />
                  </li>
                ))}
              </ul>
            </div>
          </div>
        </>
      );
    }
    return dataList;
  }

  render() {
    const pageNumbers = [];
    let maxPage = Math.ceil(
      this.state.card.length / this.state.imagesPerPage
    );
    for (let i = 1; i <= maxPage; i++) {
      pageNumbers.push(i);
    }
    const renderPageNumbers = pageNumbers.map((number) => {
      return (
        <li key={number} id={number} onClick={this.handleClick}>
          {number}
        </li>
      );
    });

    return (
      <>
        {this.renderMenu()}
        <div className="pagination">
          {/* <ul className="page-numbers">
            <li onClick={() => this.setState({ currentPage: 1 })}>First</li>
            {renderPageNumbers}
            <li onClick={() => this.setState({ currentPage: maxPage })}>
              Last
            </li>
          </ul> */}
        </div>
      </>
    );
  }
}
export default MainContent;
