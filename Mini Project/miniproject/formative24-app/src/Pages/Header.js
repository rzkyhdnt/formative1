import React from "react";
import HeadNav from '../Components/HeadNav'


class Header extends React.Component {
  render() {
    return (
      <div id="header">
        <HeadNav />
      </div>
    );
  }
}

export default Header;
