import logo from './logo.svg';
import './App.css';
import Header from './Pages/Header'
import './Assets/css/style.css'
import AdBox from './Components/AdBox';
import MainContent from './Components/MainContent';
import Footer from './Components/Footer';

function App() {
  return (
    <div className="App">
      <Header />
      <AdBox />
      <MainContent />
      <Footer />
    </div>
  );
}

export default App;
