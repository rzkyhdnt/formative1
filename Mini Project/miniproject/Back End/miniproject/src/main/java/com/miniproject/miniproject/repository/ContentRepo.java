package com.miniproject.miniproject.repository;

import com.miniproject.miniproject.model.Content;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ContentRepo extends JpaRepository<Content, Integer> {
    
}
