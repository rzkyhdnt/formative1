package com.nexsoft;

import java.util.Comparator;
import java.util.Date;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        //Objek dari bioskop dan menampilkan informasi dari bioskop
	    Bioskop cinema21 = new Bioskop();
	    cinema21.setBioskop("Cinema21", "Scientia Boulvard, Tangerang Selatan");
        cinema21.getBioskop();

        //Objek dari Cashier dan menampilkan informasi dari Cashier
        Cashier amel = new Cashier("Amel");
        amel.startDate();
        amel.getUniform();
        amel.getSalary();
        amel.getStartWorkingHour(8);
        amel.getEndWorkingHour(17);
        amel.setHasilPenjualan(30);

        //Objek dari Security dan menampilkan informasi dari Security
        Security darto = new Security("Darto");
        darto.startDate();
        darto.getUniform();
        darto.getSalary();
        darto.getStartWorkingHour(13);
        darto.getEndWorkingHour(21);
        darto.setCertification();
        darto.setSecurityGears();

        //Objek dari Cleaning dan menampilkan informasi dari Cleaning
        Cleaning hari = new Cleaning("Hari");
        hari.startDate();
        hari.getUniform();
        hari.getSalary();
        hari.getStartWorkingHour(7);
        hari.getEndWorkingHour(15);
        hari.getCleaningGears();

        //Objek dari Studio dan menampilkan informasi dari Studio
        Studio studioInfo = new Studio();
        studioInfo.filmInfo();

    }
}
