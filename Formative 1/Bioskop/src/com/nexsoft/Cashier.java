package com.nexsoft;

import java.util.Date;

public class Cashier extends Employee {
    String name;
    int tiket, hasilPenjualan;

    public Cashier(String name){
        this.name = name;
        System.out.println("\n-----Employee Information (Cashier)-----");
        System.out.println("Nama Kasir      : " + name);
    }

    public void setHasilPenjualan(int tiket){
        this.tiket = tiket;
        hasilPenjualan = tiket * 55000;
        System.out.println("Tiket yang berhasil dijual sebanyak " + tiket + " tiket");
        System.out.println("Hasil penjualan total yang didapatkan adalah " + hasilPenjualan + " Rupiah");
    }

    @Override
    public void getSalary() {
        System.out.println("Gaji            : Rp. 4.500.000");
    }

    @Override
    public void getUniform() {
        System.out.println("Pakaian Kerja   : Dress Hitam, Rok Hitam");
    }

    @Override
    public void getStartWorkingHour(int startWorkingHour) {
        if(startWorkingHour <= 12){
            System.out.println("Jam Mulai Kerja : Pukul " + startWorkingHour + " Pagi");
        } else if (startWorkingHour > 12 && startWorkingHour <= 16){
            System.out.println("Jam Mulai Kerja : Pukul " + startWorkingHour + " Siang");
        } else {
            System.out.println("Cashier hanya memiliki 2 shift");
        }
    }

    @Override
    public void getEndWorkingHour(int endWorkingHour) {
        if(endWorkingHour <= 15){
            System.out.println("Jam Pulang      : Pukul " + endWorkingHour + " Siang");
        } else if(endWorkingHour > 15 && endWorkingHour <= 19){
            System.out.println("Jam Pulang      : Pukul " + endWorkingHour + " Petang");
        } else {
            System.out.println("Jam Pulang      : Pukul " + endWorkingHour + " Malam");
        }
    }

    @Override
    public void startDate() {
        Date startDate = new Date();
        System.out.println(startDate.toString());
    }
}
