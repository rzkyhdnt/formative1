package com.nexsoft;

abstract class Employee {
    private String name, uniform;
    private int salary, startWorkingHour, endWorkingHour;

    abstract void getSalary();
    abstract void getUniform();
    abstract void getStartWorkingHour(int startWorkingHour);
    abstract void getEndWorkingHour(int endWorkingHour);
    abstract void startDate();
}
