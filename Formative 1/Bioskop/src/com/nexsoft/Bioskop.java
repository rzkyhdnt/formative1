package com.nexsoft;

public class Bioskop {
    String name, address;
    String openingHours = "Jam 10 Pagi";

    public void getBioskop(){
        System.out.println("\n---------Informasi Bioskop---------");
        System.out.println("Nama Bioskop 	        : " + name);
        System.out.println("Alamat Bioskop	        : " + address);
        System.out.println("Opening                 : " + openingHours);
    }

    public void setBioskop(String name, String address){
        this.name = name;
        this.address = address;
    }
}
