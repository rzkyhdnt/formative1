package com.nexsoft;

import java.util.Date;
import java.util.List;

public class Security extends Employee {
    String name;

    public Security(String name){
        this.name = name;
        System.out.println("\n-----Employee Information (Security)-----");
        System.out.println("Nama Security       : " + name);
    }

    public void setCertification(){
        List<String> certification = List.of("Ahli K3", "Defender Basic", "Keeping Guard");
        System.out.println("\n    Sertifikasi Security     ");
        certification.stream().forEach(System.out::println);
    }

    public void setSecurityGears(){
        List<String> securityGears = List.of("Pentungan", "Pistol", "Borgol", "Walkie Talkie");
        System.out.println("\n    Perlengkapan Security     ");
        securityGears.stream().forEach(System.out::println);
    }

    @Override
    void getSalary() {
        System.out.println("Gaji                : Rp. 3.500.000");
    }

    @Override
    void getUniform() {
        System.out.println("Pakaian Kerja       : PDH Biru Dongker, Celana Biru Dongker");
    }

    @Override
    void getStartWorkingHour(int startWorkingHour) {
        if(startWorkingHour <= 12){
            System.out.println("Jam Mulai Kerja     : Pukul " + startWorkingHour + " Pagi");
        } else if (startWorkingHour > 12 && startWorkingHour <= 18){
            System.out.println("Jam Mulai Kerja     : Pukul " + startWorkingHour + " Siang");
        } else  {
            System.out.println("Jam Mulai Kerja     : Pukul " + startWorkingHour + " Malam");
        }
    }

    @Override
    void getEndWorkingHour(int endWorkingHour) {
        if(endWorkingHour <= 15){
            System.out.println("Jam Pulang          : Pukul " + endWorkingHour + " Siang");
        } else if(endWorkingHour > 15 && endWorkingHour <= 19){
            System.out.println("Jam Pulang          : Pukul " + endWorkingHour + " Petang");
        } else {
            System.out.println("Jam Pulang          : Pukul " + endWorkingHour + " Malam");
        }
    }

    @Override
    void startDate() {
        Date startDate = new Date();
        System.out.println(startDate.toString());
    }
}
