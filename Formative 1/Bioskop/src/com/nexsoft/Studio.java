package com.nexsoft;


import java.util.HashMap;
import java.util.Map;

public class Studio extends Bioskop {
    String name;
    int seatsAmount = 60;

    public void filmInfo(){
        HashMap<Integer, String> film = new HashMap<>();
        film.put(1, "Avengers - Endgame (Rp. 55.000)");
        film.put(2, "IT (Rp. 55.000)");
        film.put(3, "Annabel 3D (Rp. 110.000)");

        System.out.println("\n---------Studio Information---------");

        for(Map.Entry b: film.entrySet()){
            System.out.println("Studio " + b.getKey() + ": " + b.getValue());
        }

        System.out.println("\nSeats yg tersedia : " + seatsAmount);
    }
}
