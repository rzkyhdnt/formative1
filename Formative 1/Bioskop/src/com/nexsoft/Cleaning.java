package com.nexsoft;

import java.util.Date;
import java.util.List;

public class Cleaning extends Employee {
    String name;

    public Cleaning(String name){
        this.name = name;
        System.out.println("\n-----Employee Information (Cleaning)-----");
        System.out.println("Nama Cleaning       : " + name);
    }


    public void getCleaningGears(){
        List<String> cleaningGears = List.of("Sapu", "Kain Pel", "Kemoceng", "Semprotan");
        System.out.println("\n    Perlengkapan Cleaning Man     ");
        cleaningGears.stream().forEach(System.out::println);
    }


    @Override
    void getSalary() {
        System.out.println("Gaji                : Rp. 3.200.000");
    }

    @Override
    void getUniform() {
        System.out.println("Pakaian Kerja       : Kemeja coklat muda, Celana coklat tua");
    }

    @Override
    void getStartWorkingHour(int startWorkingHour) {
        if(startWorkingHour <= 12){
            System.out.println("Jam Mulai Kerja     : Pukul " + startWorkingHour + " Pagi");
        } else if (startWorkingHour > 12 && startWorkingHour <= 18){
            System.out.println("Jam Mulai Kerja     : Pukul " + startWorkingHour + " Siang");
        } else  {
            System.out.println("Jam Mulai Kerja     : Pukul " + startWorkingHour + " Malam");
        }
    }

    @Override
    void getEndWorkingHour(int endWorkingHour) {
        if(endWorkingHour <= 15){
            System.out.println("Jam Pulang          : Pukul " + endWorkingHour + " Siang");
        } else if(endWorkingHour > 15 && endWorkingHour <= 19){
            System.out.println("Jam Pulang          : Pukul " + endWorkingHour + " Petang");
        } else {
            System.out.println("Jam Pulang          : Pukul " + endWorkingHour + " Malam");
        }
    }

    @Override
    void startDate() {
        Date startDate = new Date();
        System.out.println(startDate.toString());
    }
}
