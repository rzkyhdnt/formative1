package com.example.brand;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@Controller
public class ProductController {
    @Autowired
    ProductRepository productRepository;

    @Autowired
    BrandRepository brandRepository;

    public List<Brand> getAllBrands(){
        return brandRepository.findAll();
    }
    public List<Product> getAllProducts() { return productRepository.findAll();}

    @ModelAttribute
    public void addBrand(Model model){
        List<Brand> brandList = new ArrayList<Brand>(getAllBrands());
        model.addAttribute("brandList", brandList);
    }

    @ModelAttribute
    public void products(Model model){
        List<Product> productList = new ArrayList<Product>(getAllProducts());
        model.addAttribute("productList", productList);
    }

    @GetMapping("/")
    public String home(){
        return "index";
    }

    @GetMapping("/form")
    public String createProduct(Model model){
        Product product = new Product();
        model.addAttribute("product", product);
        return "form";
    }

    @GetMapping("/show")
    public String showProduct(Model model){
        return "show";
    }

    @GetMapping("/success")
    public String test(Model model){
        return "success";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") int id, Model model){
        Product product = productRepository.findById(id);
        productRepository.delete(product);
        return "redirect:/show";
    }

    @GetMapping("/details/{id}")
    public String showDetails(@PathVariable("id") int id, Model model){
        Product product = productRepository.findById(id);
        model.addAttribute("product", product);
        return "details";
    }

    @PostMapping("/form")
    public String doCreateProduct(@ModelAttribute("product") Product product){
        productRepository.save(product);
        return "success";
    }
}

