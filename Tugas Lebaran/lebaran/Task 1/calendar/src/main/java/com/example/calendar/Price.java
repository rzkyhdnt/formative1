package com.example.calendar;

import java.time.LocalDate;
import java.time.Period;

public class Price {
    final int NORMAL_PRICE = 600;
    final int DISC_SUMMER = 720;
    final int DISC_WINTER = 690;
    double totalPrice;
    LocalDate tglBrgkt;
    LocalDate timeNow = LocalDate.now();

    public Price(){

    }

    public Price(double totalPrice, LocalDate tglBrgkt, LocalDate timeNow) {
        this.totalPrice = totalPrice;
        this.tglBrgkt = tglBrgkt;
        this.timeNow = timeNow;
    }

    public LocalDate getTglBrgkt() {
        return tglBrgkt;
    }

    public void setTglBrgkt(LocalDate tglBrgkt) {
        this.tglBrgkt = tglBrgkt;
    }

    public void setTimeNow(LocalDate timeNow) {
        this.timeNow = timeNow;
    }

    public double getTotalPrice() {
        Period diff = Period.between(timeNow, tglBrgkt);
        int selisihHari = Math.abs(tglBrgkt.getDayOfMonth() - timeNow.getDayOfMonth());
        int selisihBulan = diff.getMonths();
        int selisihTahun = diff.getYears();

        if (tglBrgkt.getMonthValue() >= 6 && tglBrgkt.getMonthValue() <= 9) {
            if (selisihBulan >= 10) {
                totalPrice = DISC_SUMMER - (DISC_SUMMER * 0.15);
                return totalPrice;
            } else if(selisihBulan == 0 && selisihTahun == 1){
                totalPrice = DISC_SUMMER - (DISC_SUMMER * 0.15);
                return totalPrice;
            } else if (selisihBulan >= 6 && selisihBulan <= 9) {
                totalPrice = DISC_SUMMER - (DISC_SUMMER * 0.025);
                return totalPrice;
            } else if (selisihBulan == 0 && selisihTahun == 0) {
                if (selisihHari >= 2 && selisihHari <= 5) {
                    totalPrice = DISC_SUMMER + (DISC_SUMMER * 0.1);
                    return totalPrice;
                } else if (selisihHari == 0) {
                    totalPrice = DISC_SUMMER + (DISC_SUMMER * 0.2);
                } else {
                    totalPrice = DISC_SUMMER;
                    return totalPrice;
                }
            } else {
                totalPrice = DISC_SUMMER;
                return totalPrice;
            }
        } else if (tglBrgkt.getMonthValue() <= 3 && tglBrgkt.getMonthValue() == 12) {
            if (selisihBulan >= 10) {
                totalPrice = DISC_WINTER - (DISC_WINTER * 0.15);
                return totalPrice;
            } else if(selisihBulan == 0 && selisihTahun == 1){
                totalPrice = DISC_WINTER - (DISC_WINTER * 0.15);
                return totalPrice;
            } else if (selisihBulan >= 6 && selisihBulan <= 9) {
                totalPrice = DISC_WINTER - (DISC_WINTER * 0.025);
                return totalPrice;
            } else if (selisihBulan == 0 && selisihTahun == 0) {
                if (selisihHari >= 2 && selisihHari <= 5) {
                    totalPrice = DISC_WINTER + (DISC_WINTER * 0.1);
                    return totalPrice;
                } else if (selisihHari == 0) {
                    totalPrice = DISC_WINTER + (DISC_WINTER * 0.2);
                } else {
                    totalPrice = DISC_WINTER;
                    return totalPrice;
                }
            } else {
                totalPrice = DISC_WINTER;
                return totalPrice;
            }
        } else {
            if (selisihBulan >= 10) {
                totalPrice = NORMAL_PRICE - (NORMAL_PRICE * 0.15);
                return totalPrice;
            } else if(selisihBulan == 0 && selisihTahun == 1){
                totalPrice = NORMAL_PRICE - (NORMAL_PRICE * 0.15);
                return totalPrice;
            } else if (selisihBulan >= 6 && selisihBulan <= 9) {
                totalPrice = NORMAL_PRICE - (NORMAL_PRICE * 0.025);
                return totalPrice;
            } else if (selisihBulan == 0 && selisihTahun == 0) {
                if (selisihHari >= 2 && selisihHari <= 5) {
                    totalPrice = NORMAL_PRICE + (NORMAL_PRICE * 0.1);
                    return totalPrice;
                } else if (selisihHari == 0) {
                    totalPrice = NORMAL_PRICE+ (NORMAL_PRICE * 0.2);
                } else {
                    totalPrice = NORMAL_PRICE;
                    return totalPrice;
                }
            } else {
                totalPrice = NORMAL_PRICE;
                return totalPrice;
            }
        }
        return totalPrice;
    }


}
