package com.example.calendar;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.lang.reflect.Array;
import java.time.LocalDate;
import java.util.ArrayList;

@Controller
public class MainController {


    @GetMapping("/")
    public String show(Model model){
        model.addAttribute("myValue", "myValue");
        ArrayList date = new ArrayList();
        Price price = new Price();
        int day = 16;
        int month = 5;
        int year = 2021;
        boolean loop = true;
        price.setTglBrgkt(LocalDate.of(year, month, day));

        while(loop){
            if(!(day == 16 && month == 05 && year == 2022)) {
                switch (month) {
                    case 1:
                    case 3:
                    case 5:
                    case 7:
                    case 8:
                    case 10:
                        if (day == 31) {
                            day = 1;
                            month += 1;
                            price.setTglBrgkt(LocalDate.of(year, month, day ));
                            date.add(price.getTotalPrice());
                        } else {
                            day += 1;
                            price.setTglBrgkt(LocalDate.of(year, month, day));
                            date.add(price.getTotalPrice());
                        }
                        break;

                    case 2:
                        if (day == 28) {
                            day = 1;
                            month += 1;
                            price.setTglBrgkt(LocalDate.of(year, month, day));
                            date.add(price.getTotalPrice());
                        } else {
                            day += 1;
                            price.setTglBrgkt(LocalDate.of(year, month, day));
                            date.add(price.getTotalPrice());
                        }

                        break;

                    case 4:
                    case 6:
                    case 9:
                    case 11:
                        if (day == 30) {
                            day = 1;
                            month += 1;
                            price.setTglBrgkt(LocalDate.of(year, month, day));
                            date.add(price.getTotalPrice());
                        } else {
                            day += 1;
                            price.setTglBrgkt(LocalDate.of(year, month, day));
                            date.add(price.getTotalPrice());
                        }
                        break;

                    case 12:
                        if (day == 31) {
                            day = 1;
                            month += 1;
                            price.setTglBrgkt(LocalDate.of(year, month, day));
                            date.add(price.getTotalPrice());
                        } else if (month == 12) {
                            day = 1;
                            month = 1;
                            year += 1;
                            price.setTglBrgkt(LocalDate.of(year, month, day));
                            date.add(price.getTotalPrice());
                        } else {
                            day += 1;
                            price.setTglBrgkt(LocalDate.of(year, month, day));
                            date.add(price.getTotalPrice());
                        }
                        break;
                }
            } else {
                loop = false;
            }
        }
        model.addAttribute("date", date);
        return "index";
    }
}



