package com.example.formative15;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.sql.Date;

@Entity
public class District {
    String code;
    String name;
    Date deleteDate;
    @Id
    int id;
    int countryId;
    int cityId;
    int provinceId;

    public District(){

    }

    public District(String code, String name, Date deleteDate, int id, int countryId, int cityId, int provinceId){
        this.code = code;
        this.name = name;
        this.deleteDate = deleteDate;
        this.id = id;
        this.countryId = countryId;
        this.cityId = cityId;
        this.provinceId = provinceId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDeleteDate() {
        return deleteDate;
    }

    public void setDeleteDate(Date deleteDate) {
        this.deleteDate = deleteDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCountryId() {
        return countryId;
    }

    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    public int getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(int provinceId) {
        this.provinceId = provinceId;
    }
}
