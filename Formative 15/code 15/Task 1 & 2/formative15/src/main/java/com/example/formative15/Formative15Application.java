package com.example.formative15;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Formative15Application {

	public static void main(String[] args) {
		SpringApplication.run(Formative15Application.class, args);
	}

}
