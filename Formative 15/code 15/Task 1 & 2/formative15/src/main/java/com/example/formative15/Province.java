package com.example.formative15;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.sql.Date;
import java.util.List;

@Entity
public class Province {
    String code;
    String name;
    Date deleteDate;
    @Id
    int id;
    int countryId;
    @OneToMany(targetEntity = City.class)
    List<City>cities;

    public Province(){

    }

    public Province(String code, String name, Date deleteDate, int id, int countryId, List<City> cities){
        super();
        this.code = code;
        this.name = name;
        this.deleteDate = deleteDate;
        this.id = id;
        this.countryId = countryId;
        this.cities = cities;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDeleteDate() {
        return deleteDate;
    }

    public void setDeleteDate(Date deleteDate) {
        this.deleteDate = deleteDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCountryId() {
        return countryId;
    }

    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }

    public List<City> getCities() {
        return cities;
    }

    public void setCities(List<City> cities) {
        this.cities = cities;
    }

    public List<City> getCity() {
        return cities;
    }
}
