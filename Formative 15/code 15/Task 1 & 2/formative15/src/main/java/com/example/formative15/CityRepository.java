package com.example.formative15;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CityRepository extends CrudRepository<City, Integer> {
    City findById(int id);
    List<City> findAll();
    void deleteById(int id);
    City save(City city);
}
