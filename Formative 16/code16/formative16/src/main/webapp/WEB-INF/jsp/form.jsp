<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" isELIgnored="false" %>
<!DOCTYPE html>
<html lang="en" xmlns:th="http://www.thymeleaf.org">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <title>Test</title>
    <link rel="stylesheet" th:href="@{/css/bootstrap.min.css}" media="screen">
    <link rel="stylesheet" th:href="@{/css/stylesheet.css}" media="screen">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

</head>
<body>
    <div class="container-fluid px-1 py-5 mx-auto">
        <div class="row d-flex justify-content-center">
            <div class="col-xl-7 col-lg-8 col-md-9 col-11 text-center">
                <h3>FORM</h3>
                <div class="card">
                    <form action="success.html" th:action="@{/form}" th:object="${price}" method="post">
                        <div class="row justify-content-between text-left">
                            <div class="form-group col-sm-6 flex-column d-flex"><label class="form-control-label px-3">Recipient</label> <input type="text" th:field ="*{recipent}" name="recipient"> </div>
                            <div class="form-group col-sm-6 flex-column d-flex"><label class="form-control-label px-3">Male </label> <input type="radio" name="gender" th:field ="*{gender}" value="male">
                                <label class="form-control-label px-3">Female</label> <input type="radio" name="gender" th:field ="*{gender}" value="female"> </div>
                        </div>
                        <div class="row justify-content-between text-left">
                            <div class="form-group col-sm-6 flex-column d-flex"><label class="form-control-label px-3">Street</label> <input type="text" name="street" th:field ="*{street}"></div>
                            <div class="form-group col-sm-6 flex-column d-flex"><label class="form-control-label px-3">Zipcode : </label> <input type="text" th:field ="*{zipCode}" name="zipcode"></div>
                            <div class="form-group col-sm-6 flex-column d-flex"><label class="form-control-label px-3">District : </label> <input type="text" name="district" th:field ="*{district}"></div>
                        </div>
                        <div class="row justify-content-between text-left">
                            <div class="form-group col-sm-6 flex-column d-flex"><label class="form-control-label px-3">City : </label> <input type="text" name="city" th:field ="*{city}"></div>
                            <div class="form-group col-sm-6 flex-column d-flex"><label class="form-control-label px-3">Province : </label> <input type="text" name="province" th:field ="*{province}"></div>
                        </div>
                        <div class="row justify-content-between text-left">
                            <div class="form-group col-sm-6 flex-column d-flex"> <label>Country :</label> <select class="custom-select" th:field="*{country}">
                                <option th:each = "count : ${countries}"
                                        th:value = "${count.id}"
                                        th:text = "${count.name}"/></select></div>
                            <div class="form-group col-sm-6 flex-column d-flex"><label>Assurance :</label> <input type="checkbox" value="true" th:field ="*{assurance}" id="assurance" onclick="change()"> </div>
                        </div>
                        <div class="row justify-content-between text-left">
                            <div class="form-group col-sm-6 flex-column d-flex"><label>Description :</label> <textarea name="description" cols="30" rows="10" th:field ="*{description}"></textarea></div>
                        </div>
                        <div class="row justify-content-between text-left">
                            <div class="form-group col-sm-6 flex-column d-flex"><label>Total Price</label> <input type="text"  readonly value="Rp. 500.000" th:field="*{totalPrice}"  id="price"></div>
                            <div class="form-group col-sm-6 flex-column d-flex"><label>Delivery Services Cost</label> <input type="text" readonly value="Rp. 50.000"  th:field="*{deliveryCost}" id="delivery"></div>
                        </div>
                        <div class="row justify-content-end">
                            <div class="form-group col-12 mt-4"> <button type="submit" class="btn-block btn-primary">Add</button> </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>

    <script th:inline="javascript">
        function change(){
            var y = document.getElementById("price").value = "Rp. 445.000";
            var z = document.getElementById("delivery").value = "Rp. 45.000";
        }
    </script>
</body>
</html>