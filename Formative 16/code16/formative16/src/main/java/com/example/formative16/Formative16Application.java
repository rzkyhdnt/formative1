package com.example.formative16;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class Formative16Application {

	public static void main(String[] args) {
		SpringApplication.run(Formative16Application.class, args);
	}
}
