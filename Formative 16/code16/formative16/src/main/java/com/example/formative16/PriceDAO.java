package com.example.formative16;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PriceDAO extends JpaRepository<Price, Integer> {

}
