package com.example.formative16;

import javax.persistence.*;

@Entity
public class Price {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String recipent;
    private String gender;
    private String street;
    private int zipCode;
    private String district;
    private String city;
    private String province;

    @ManyToOne(targetEntity = Country.class, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "country_id")
    private Country country;
    private boolean assurance;
    private String description;
    private String totalPrice;
    private String deliveryCost;

    public Price(){

    }

    public Price(int id, String recipent, String gender, String street, int zipCode, String district, String city,
                 String province, Country country, boolean assurance, String description, String totalPrice, String deliveryCost) {
        this.id = id;
        this.recipent = recipent;
        this.gender = gender;
        this.street = street;
        this.zipCode = zipCode;
        this.district = district;
        this.city = city;
        this.province = province;
        this.country = country;
        this.assurance = assurance;
        this.description = description;
        this.totalPrice = totalPrice;
        this.deliveryCost = deliveryCost;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRecipent() {
        return recipent;
    }

    public void setRecipent(String recipent) {
        this.recipent = recipent;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public int getZipCode() {
        return zipCode;
    }

    public void setZipCode(int zipCode) {
        this.zipCode = zipCode;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public boolean isAssurance() {
        return assurance;
    }

    public void setAssurance(boolean assurance) {
        this.assurance = assurance;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getDeliveryCost() {
        return deliveryCost;
    }

    public void setDeliveryCost(String deliveryCost) {
        this.deliveryCost = deliveryCost;
    }
}
